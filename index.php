<!DOCTYPE html>
<html lang="en">
<head>
	<title>TakeHomePay Calculator</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="bootstrap.min.css">
	<script src="jquery.min.js"></script>
	<script src="bootstrap.min.js"></script>
	<style type="text/css">
		.btn.calculate { color: #fff; background-color: #21a9e1; border-color: #0d7ca9; display: block; width: 100%; font-weight: 700; font-size: 16px;}
		.btn { border: 1px solid #ccc; background-color: #fff; }
		#incomeAfter { background-color: #fff; min-height: 250px; border: 1px solid #e3e4e4; border-radius: 3px; padding: 30px;}
		.myCalc{ padding: 30px; background: #f4f5f5; border: 1px solid #e3e4e4; border-radius: 3px;}
		.thp-result {
			font-family: monospace; font-size: 20px; text-align: center; text-transform: uppercase; line-height: 22px; }
		.thp-amount { font-family: cursive; font-weight: bolder; font-size: 22px; text-align: center; line-height: 22px; color: #ffffff; border: 1px solid #21a9e1; padding: 10px; background: #21a9de; border-radius: 4px;}
		.loader { margin-top: 15px; margin-left: 106px; }
	</style>
</head>
<body>

	<div class="container">
		<h1>TakeHomePay Calculator</h1>
		<hr>
		<div class="myCalc">
			<div class="row">
				<div class="col-sm-7">
					<div class="btn-group">
						<button type="button" onclick="setType('limited')" class="btn active">Ltd Company</button>
						<button type="button" onclick="setType('soleTrader')" class="btn">Sole Trader</button>
						<button type="button" onclick="setType('umbrella')" class="btn">Umbrella</button>
					</div>
					<br><br>
					<div class="row">
						<div class="col-sm-6 form-group">
							<label class="control-label" for="income">Income (per day)</label>
							<input class="form-control" id="income" name="income" placeholder="£350" type="number">
						</div>
						<div class="col-sm-6 form-group">
							<label class="control-label" for="expenses">Annual expenses</label>
							<input class="form-control" id="expenses" name="expenses" placeholder="£6000" type="number">
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6 form-group">
							<label class="control-label" for="workingWeeks">Working weeks per year</label>
							<input class="form-control" id="workingWeeks" name="workingWeeks" placeholder="48" type="number">
							<input type="hidden" id="companyType" value="limited">
						</div>
					</div>
					<div class="row">
						<div class="col-sm-6 form-group">
							<button class="btn btn-default calculate" id="calculate" type="button">Calculate</button>
						</div>
					</div>
				</div>
				<div class="col-sm-5">
					<div id="incomeAfter"></div>
				</div>
			</div>
		</div>
	</div>
</body>
<script>
	function setType(type) {
		$('#companyType').val(type);
	}
	$(document).ready(function(){
		$('#calculate').click(function(){
			var income = $('#income').val();
			var expenses = $('#expenses').val();
			var workingWeeks = $('#workingWeeks').val();
			var companyType = $('#companyType').val();
			if(income > 0 && expenses > 0 && workingWeeks > 0){
				$('#incomeAfter').html('<img src="loader.gif" class="loader"/>');
				$.ajax({
					type: "POST",
					url: "calculate.php",
					data: {income, expenses, workingWeeks, companyType},
					success: function(result){
						$('#incomeAfter').html(result);
  					// console.log(result);
  				}
  			});
			} else {
				alert("Please provide valid values.");
			}
		});

		$(".btn-group > .btn").click(function(){
			$(this).addClass("active").siblings().removeClass("active");
		});
	});
</script>
</html>