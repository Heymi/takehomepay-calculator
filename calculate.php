<?php 

require_once 'formulas.php';

@$income = isset($_REQUEST['income']) ? $_REQUEST['income'] : false;
@$expenses = isset($_REQUEST['expenses']) ? $_REQUEST['expenses'] : false;
@$workingWeeks = isset($_REQUEST['workingWeeks']) ? $_REQUEST['workingWeeks'] : false;
@$companyType = isset($_REQUEST['companyType']) ? $_REQUEST['companyType'] : false;

$formula = new Formula();

$takeHomePay = $formula->getResultantTHP($income, $expenses, $workingWeeks, $companyType);

if($companyType != 'umbrella') {

	$thpUmbrella = $formula->getResultantTHP($income, $expenses, $workingWeeks, 'umbrella');

	if ($companyType == 'soleTrader') {

		$bobSoleTrader = $formula->betterOffBySoleTrader($takeHomePay, $thpUmbrella);
		$betterOffBy = number_format($bobSoleTrader, 2, '.', '');

	} elseif ($companyType == 'limited') {

		$bobLimited = $formula->betterOffByLimited($takeHomePay, $thpUmbrella);
		$betterOffBy = number_format($bobLimited, 2, '.', '');

	}

}

if ($companyType == 'umbrella') {
	$c = "an Umbrella";
} elseif ($companyType == 'soleTrader') {
	$c = "a Sole Trader";
} else {
	$c = "a Limited Company";
}

$takeHomePay = number_format($takeHomePay, 2, '.', '');

if($companyType != 'umbrella') {
	echo "<p class='thp-result'>As ".$c." your Take Home Pay is <p class='thp-amount'>£".$takeHomePay."</p></p><p class='thp-result'>with Better Off By:<p class='thp-amount'>£".$betterOffBy."</p></p>";
} else {
	echo "<p class='thp-result'>As ".$c." your Take Home Pay is <p class='thp-amount'>£".$takeHomePay."</p></p>";
}