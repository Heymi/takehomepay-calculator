<?php

/**
* Formula class contains all the formulas we use in the calculator.
* Developer : Humayun Ehtisham
*/
class Formula
{
	// Annual income
	public function annualIncome($perday, $numOfWeeksWorkedPerYear)
	{
		$weekDays = 5;
		$perWeek = $perday * $weekDays;
		return $perWeek * $numOfWeeksWorkedPerYear;
	}

	// Annual income after expenses
	public function annualIncomeAfterExpense($annualIncome, $expense)
	{
		return $annualIncome - $expense;
	}

	// Income including Flat rate VAt income
	public function flatVatIncome($annualIncome, $companyType)
	{
		if ($companyType == 'umbrella') {
			return $annualIncome;
		}
		elseif ($companyType == 'soleTrader' || $companyType == 'limited') {
			return $annualIncome * 1.2 - (0.135 * $annualIncome * 1.2);
		}

	}

	// Total expenses
	public function totalExpenses($expenses, $companyType)
	{
		$salary = 0;
		$nationalHomeOfficeCost = 0;
		$expense = 0;

		if ($companyType == 'umbrella') {
			$expense = $expenses;
			return $expense + $nationalHomeOfficeCost + $salary;
		}
		elseif ($companyType == 'soleTrader') {
			$expense = 6000;
			return $expense + $nationalHomeOfficeCost + $salary;
		}
		elseif ($companyType == 'limited') {
			$salary = 8060 * 2; // Husband & Wife
			$nationalHomeOfficeCost = 1000;
			$expense = 7000;
			return $expense + $nationalHomeOfficeCost + $salary;
		}

	}

	// Profit
	public function profit($vatIncome, $totalExpense)
	{
		return $vatIncome - $totalExpense;
	}

	// Profit after Corporation Tax
	public function profitAfterCorpTax($profit, $companyType)
	{
		if ($companyType == 'limited') {
			$tax = $profit * 0.2; // Corporation Tax at 20%
			return $profit - $tax;
		} else {
			return "Only Acconomy Company Limited is Valid.";
		}

	}

	// Subject to personal tax
	public function profitAfterPersonalTax($profitAfterCorpTax)
	{
		if (gettype($profitAfterCorpTax) != 'string') {
			return $profitAfterCorpTax / 2; // (assumes husband and wife own 50% each of shares)
		} else {
			return "String type values are invalid!";
		}

	}

	// Dividend Tax
	public function dividendTax($by, $afterPersonalTax)
	{
		$dividendTax = 0;
		if (gettype($afterPersonalTax) != 'string') {

			if ($by == 0.075 || $by == '7.5%') {

				$by = 0.075;
				if($afterPersonalTax - 5000 > 0) {
					$dividendTax = ($afterPersonalTax - 5000) * $by * 2;
				}

			} elseif ($by == 0.325 || $by == '32.5%') {

				$by = 0.325;
				if($afterPersonalTax + 8060 < 43000) {

					$dividendTax = 0;

				} else {

					$dividendTax = (( ($afterPersonalTax + 8060) - 43000 ) * 0.25) * 2;

				}

			} elseif ($by == 0.381 || $by == '38.1%') {

				$by = 0.381;
				$dividendTax = 0;

			}

			return $dividendTax;

		} else {
			return "String type values are invalid!";
		}

	}

	// Total Dividend Tax
	public function totalDividendTax($dividendSevenHalf, $dividendThirtyTwoHalf, $dividendThirtyEight)
	{
		return $dividendSevenHalf + $dividendThirtyTwoHalf + $dividendThirtyEight;
	}

	// Salary of Umbrella Company
	public function salary($profit, $companyType)
	{
		if ($companyType == 'umbrella') {
			return ( $profit + 1120.91 ) / 1.138;
		} else {
			return false;
		}

	}

	// ERS NI of Umbrella Company
	public function ersNI($salary)
	{
		if ($salary != false) {
			return ($salary - 8122.52) * 0.138;
		} else {
			return "Values are invalid!";
		}

	}

	// EES NI of Umbrella Company
	public function eesNI($salary)
	{
		$eesHigherRate = 0.12; // Class 1 NI EES higher rate
		$eesThresholdClass = 8060; // Threshold Class 1 EES NI
		$eesUpperLimit = 43004; // Upper limit Class 1 EES NI

		if ($salary != false) {

			if ($salary > $eesUpperLimit) {
				return ($eesUpperLimit - $eesThresholdClass) * $eesHigherRate;
			} else {
				return ($salary - $eesThresholdClass) * $eesHigherRate;
			}

		} else {
			return "String type values are invalid!";
		}

	}

	// ees ni @2% of Umbrella Company
	public function eesAtTwo($salary)
	{
		$eesHigherRate = 0.12; // Class 1 NI EES higher rate
		$eesThresholdClass = 8060; // Threshold Class 1 EES NI
		$eesUpperLimit = 43004; // Upper limit Class 1 EES NI

		if ($salary != false) {

			if ($salary > $eesUpperLimit) {
				return 0.02 * ($salary - $eesUpperLimit);
			} else {
				return 0;
			}

		} else {
			return "String type values are invalid!";
		}

	}

	// Basic rate tax
	public function basicIncomeTax($salary, $companyType)
	{
		$basicRateTax = 0.2; // 20%
		$persAllowance = 11000; // Personal allowance

		if ($companyType == 'umbrella') {
			if($salary < $persAllowance) {
				return 0;
			} else {
				return ($salary - $persAllowance) * $basicRateTax;
			}
		} elseif ($companyType == 'soleTrader') {
			$profit = $salary;
			if($profit < $persAllowance) {
				return 0;
			} else {
				return $profit * $basicRateTax;
			}
		}
	}

	// Higher rate tax
	public function highIncomeTax($salary, $companyType)
	{
		$basicRateTax = 0.2; // 20%
		$highRateTax = 0.4; // 40%
		$highRateThreshold = 43000; // Higher rate thershold

		if ($companyType == 'umbrella') {
			
			if($salary < $highRateThreshold) {
				return 0;
			} else {
				return ($salary - $highRateThreshold) * ($highRateTax - $basicRateTax);
			}

		} elseif ($companyType == 'soleTrader') {
			$profit = $salary;
			if($profit < $highRateThreshold) {
				return 0;
			} else {
				return ($profit - $highRateThreshold) * ($highRateTax - $basicRateTax);
			}
		}
	}

	// Class 4 NI (self employed)
	public function niClassFour($by, $profit, $companyType)
	{
		$eesThresholdClass = 8060; // Threshold Class 1 EES NI
		$eesUpperLimit = 43004; // Upper limit Class 1 EES NI
		$lowRateThreshold = 43000; // Threshold for lower rate
		if ($companyType == 'soleTrader') {

			if ($by == 0.09 || $by == '9%') {
				$by = 0.09; // 9%
				if ($profit > $eesUpperLimit) {
					return ($eesUpperLimit - $eesThresholdClass) * $by;
				} else {
					return ($profit - $eesThresholdClass) * $by;
				}
			} elseif ($by == 0.02 || $by == '2%') {
				$by = 0.02; // 2%
				if ($profit > $eesUpperLimit) {
					return ($profit - $lowRateThreshold) * $by;
				} else {
					return 0;
				}
			}

		}

	}

	// Take Home Pay
	public function takeHomePayUmbrella($salary, $ersNI, $eesNI, $eesAtTwo, $basicIncomeTax, $highIncomeTax)
	{
		return $salary - $ersNI - $eesNI - $eesAtTwo - $basicIncomeTax - $highIncomeTax;
	}

	public function takeHomePaySoleTrader($profit, $basicIncomeTax, $highIncomeTax, $niClassFour_ninePercent, $niClassFour_twoPercent)
	{
		return $profit - $basicIncomeTax - $highIncomeTax - $niClassFour_ninePercent - $niClassFour_twoPercent;
	}

	public function takeHomePayLimited($profitAfterCorpTax, $totalDividendTax)
	{
		$sal = 8060 * 2; // Husband & Wife
		return $profitAfterCorpTax - $totalDividendTax + $sal;
	}

	// Better Off By
	public function betterOffBySoleTrader($takeHomePaySoleTrader, $takeHomePayUmbrella)
	{
		return $takeHomePaySoleTrader - $takeHomePayUmbrella;		
	}

	public function betterOffByLimited($takeHomePayLimited, $takeHomePayUmbrella)
	{
		return $takeHomePayLimited - $takeHomePayUmbrella;
	}

	public function getResultantTHP($income, $expenses, $workingWeeks, $companyType)
	{

		$annualIncome = $this->annualIncome($income, $workingWeeks);

		$flatVatIncome = $this->flatVatIncome($annualIncome, $companyType);

		$totalExpenses = $this->totalExpenses($expenses, $companyType);

		$profit = $this->profit($flatVatIncome, $totalExpenses);

		$profitAfterCorpTax = $this->profitAfterCorpTax($profit, $companyType);

		$profitAfterPersonalTax = $this->profitAfterPersonalTax($profitAfterCorpTax);

		$dividendSevenHalf = $this->dividendTax('7.5%', $profitAfterPersonalTax);

		$dividendThirtyTwoHalf = $this->dividendTax('32.5%', $profitAfterPersonalTax);

		$dividendThirtyEight = $this->dividendTax('38.1%', $profitAfterPersonalTax);

		$totalDividendTax = $this->totalDividendTax($dividendSevenHalf, $dividendThirtyTwoHalf, $dividendThirtyEight);

		$salary = $this->salary($profit, $companyType);

		$ersNI = $this->ersNI($salary);

		$eesNI = $this->eesNI($salary);

		$eesAtTwo = $this->eesAtTwo($salary);

		$salaryORprofit = ($salary != false) ? $salary : $profit;

		$basicIncomeTax = $this->basicIncomeTax($salaryORprofit, $companyType);

		$highIncomeTax = $this->highIncomeTax($salaryORprofit, $companyType);

		$niClassFour_ninePercent = $this->niClassFour('9%', $profit, $companyType);

		$niClassFour_twoPercent = $this->niClassFour('2%', $profit, $companyType);

		if ($companyType == 'umbrella') {

			$takeHomePay = $this->takeHomePayUmbrella($salary, $ersNI, $eesNI, $eesAtTwo, $basicIncomeTax, $highIncomeTax);
			return $takeHomePay;

		} elseif ($companyType == 'soleTrader') {

			$takeHomePay = $this->takeHomePaySoleTrader($profit, $basicIncomeTax, $highIncomeTax, $niClassFour_ninePercent, $niClassFour_twoPercent);
			return $takeHomePay;

		} elseif ($companyType == 'limited') {

			$takeHomePay = $this->takeHomePayLimited($profitAfterCorpTax, $totalDividendTax);
			return $takeHomePay;

		}

	}

}